// check act
var crd_id = new Array(
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"10",
	"11",
	"12",
	"13",
	"14",
	"15",
	"16",
	"17",
	"18",
	"19",
	"20"
);

//ShowTime
var dayarray = new Array(
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday"
);
var montharray = new Array(
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
);
function startTime() {
	var mydate = new Date();
	var year = mydate.getYear();
	if (year < 1000) year += 1900;
	var day = mydate.getDay();
	var month = mydate.getMonth();
	var daym = mydate.getDate();
	var h = mydate.getHours();
	var m = mydate.getMinutes();
	var s = mydate.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	document.getElementById("showTime").innerHTML =
		dayarray[day] +
		"  " +
		daym +
		"  " +
		montharray[month] +
		"  " +
		year +
		"<br>" +
		h +
		" : " +
		m +
		" : " +
		s;
	var t = setTimeout(startTime, 1000);
}
function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	} // add zero in front of numbers < 10
	return i;
}

//FUNCTION Search card no.
function searchComputer() {
	// Declare variables
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("myInput");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable");
	tr = table.getElementsByTagName("tr");
	// Loop through all table rows, and hide those who don't match the search query
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
			txtValue = td.textContent || td.innerText;
			console.log("txtValue: ", txtValue);
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

//Side nav
function openNav() {
	document.getElementById("mySidenav").style.width = "230px";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
}

//active btn disabled
function activeCrd(tel, actID, actbtn, paybtn, delbtn, clrbtn) {
	document.getElementById(actbtn).disabled = true;
	document.getElementById(paybtn).disabled = false;
	document.getElementById(delbtn).disabled = false;
	document.getElementById(clrbtn).disabled = true;
	var t = new Date();
	var year = t.getYear();
	if (year < 1000) year += 1900;
	var month = t.getMonth();
	var daym = t.getDate();
	var h = checkTime(t.getHours());
	var m = checkTime(t.getMinutes());
	var s = checkTime(t.getSeconds());
	var tel = document.getElementById(tel).value;
	if (tel.length !== 10 || tel === null) {
		document.getElementById(actID).innerHTML = "WRONG TELEPHONE NUMBER";
		document.getElementById(paybtn).disabled = true;
	} else {
		document.getElementById(actID).innerHTML =
			"Tel : " +
			tel +
			"<br>" +
			"Start : " +
			daym +
			" " +
			montharray[month] +
			" " +
			year +
			" , " +
			h +
			" : " +
			m +
			" : " +
			s;
	}
}

//pay //btn disabled
function payCrd(payID, actbtn, paybtn, delbtn, clrbtn) {
	document.getElementById(actbtn).disabled = true;
	document.getElementById(paybtn).disabled = true;
	document.getElementById(delbtn).disabled = true;
	document.getElementById(clrbtn).disabled = false;
	var t = new Date();
	var year = t.getYear();
	if (year < 1000) year += 1900;
	var month = t.getMonth();
	var daym = t.getDate();
	var h = checkTime(t.getHours());
	var m = checkTime(t.getMinutes());
	var s = checkTime(t.getSeconds());
	document.getElementById(payID).innerHTML =
		"Stop : " +
		daym +
		" " +
		montharray[month] +
		" " +
		year +
		" , " +
		h +
		" : " +
		m +
		" : " +
		s;
}

//Delete Card
function deleteCrd(actID, actbtn, paybtn, delbtn, clrbtn) {
	document.getElementById(actbtn).disabled = true;
	document.getElementById(paybtn).disabled = true;
	document.getElementById(delbtn).disabled = true;
	document.getElementById(clrbtn).disabled = false;
	document.getElementById(actID).innerHTML = "START : ";
}
//Clear Card
function clear_crd(actID, payID, actbtn, paybtn, totalID) {
	document.getElementById(totalID).innerHTML = "Total : ";
	if (document.getElementById(paybtn).disabled) {
		document.getElementById(paybtn).disabled = true;
	} else {
		document.getElementById(paybtn).disabled = false;
	}
	document.getElementById(payID).innerHTML = "STOP : ";
	document.getElementById(actID).innerHTML = "START : ";
	document.getElementById(actbtn).disabled = false;
}

//Register Employee Modal
// Get the modal
var modal_reg = document.getElementById("id-modal-re-emp");

// Get the button that opens the modal
var reg_emp = document.getElementById("id-sidenav-reg-emp");

// Get the <span> element that closes the modal
var span_reg_emp = document.getElementsByClassName("close-reg-emp")[0];

// When the user clicks the button, open the modal
reg_emp.onclick = function () {
	modal_reg.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span_reg_emp.onclick = function () {
	modal_reg.style.display = "none";
};

//Search Employee
var modal_search_emp = document.getElementById("id-modal-search-emp");
var search_emp = document.getElementById("id-sidenav-search-emp");
var span_search_emp = document.getElementsByClassName("close-search-emp")[0];
search_emp.onclick = function () {
	modal_search_emp.style.display = "block";
};
span_search_emp.onclick = function () {
	modal_search_emp.style.display = "none";
};
// var submit_emp_search = document.getElementById("submit-emp-search");
// var search_modal_content = document.getElementById("search-modal-content");
// submit_emp_search.onclick = function () {
// 	modal_search_emp.style.display = "block";
// 	search_modal_content.style.display = "block";
// };

//Search Customer
var modal_search_cus = document.getElementById("id-modal-search-cus");
var search_cus = document.getElementById("id-sidenav-search-cus");
var span_search_cus = document.getElementsByClassName("close-search-cus")[0];
search_cus.onclick = function () {
	modal_search_cus.style.display = "block";
};
span_search_cus.onclick = function () {
	modal_search_cus.style.display = "none";
};

// //Search cafe
// var modal_search_cafe = document.getElementById("id-modal-search-cafe");
// var search_cafe = document.getElementById("id-sidenav-search-cafe");
// var span_search_cafe = document.getElementsByClassName("close-search-cafe")[0];
// search_cafe.onclick = function () {
// 	modal_search_cafe.style.display = "block";
// };
// span_search_cafe.onclick = function () {
// 	modal_search_cafe.style.display = "none";
// };

//Expense customer perday
var modal_expense_cus = document.getElementById("id-modal-expense-cus-perday");
var expense_cus = document.getElementById("id-sidenav-expense-cus-perday");
var span_expense_cus = document.getElementsByClassName(
	"close-expense-cus-perday"
)[0];
expense_cus.onclick = function () {
	modal_expense_cus.style.display = "block";
};
span_expense_cus.onclick = function () {
	modal_expense_cus.style.display = "none";
};

//Expense employee perday
var modal_expense_emp = document.getElementById("id-modal-expense-emp-perday");
var expense_emp = document.getElementById("id-sidenav-expense-emp-perday");
var span_expense_emp = document.getElementsByClassName(
	"close-expense-emp-perday"
)[0];
expense_emp.onclick = function () {
	modal_expense_emp.style.display = "block";
};
span_expense_emp.onclick = function () {
	modal_expense_emp.style.display = "none";
};

//Expense Daily
var modal_expense_daily = document.getElementById("id-modal-expense-daily");
var expense_daily = document.getElementById("id-sidenav-expense-daily");
var span_expense_daily = document.getElementsByClassName(
	"close-expense-daily"
)[0];
expense_daily.onclick = function () {
	modal_expense_daily.style.display = "block";
};
span_expense_daily.onclick = function () {
	modal_expense_daily.style.display = "none";
};

//Delete Customer
var modal_delete_emp = document.getElementById("id-modal-delete-emp");
var delete_emp = document.getElementById("id-sidenav-delete-emp");
var span_delete_emp = document.getElementsByClassName("close-delete-emp")[0];
delete_emp.onclick = function () {
	modal_delete_emp.style.display = "block";
};
span_delete_emp.onclick = function () {
	modal_delete_emp.style.display = "none";
};

// function getValFromEmpReg() {
// 	const empID = document.getElementById("empID").value;
// 	const fname = document.getElementById("fname").value;
// 	const lname = document.getElementById("lname").value;
// 	const phone = document.getElementById("phone").value;
// 	const gender = document.getElementById("gender").value;
// 	const address = document.getElementById("address").value;
// 	const emp = {
// 		EmpID: empID,
// 		FirstName: fname,
// 		LastName: lname,
// 		Phone: phone,
// 		Gender: gender,
// 		Address: address,
// 	};
// 	console.log(emp);
// }

// function getValFromEmpSearch() {
// 	const empID = document.getElementById("empID-search").value;
// 	console.log("Emp ID Search : ", empID);
// }

// function getValFromCusSearch() {
// 	const cusNo = document.getElementById("search-cus").value;
// 	console.log("Customer ID Search : ", cusNo);
// }
