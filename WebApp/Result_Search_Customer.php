<!--SEARCH EMPLOYEE-->
<?php $servername = "localhost";
$username = "root";
$password = "root";
$dbname = "DBpenb";

//create connection
$conn = new mysqli($servername,$username,$password,$dbname); 
?>
<?php
function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}
?>
<?php 
if(isset($_POST['btn-search-cus'])){
    $payDate = $_POST["search-cus"];
    $searchCus = $conn->query("SELECT Cus_No,Phone,Card_ID,P_Date,Login_Time,Logout_Time,Emp_ID 
    FROM customer WHERE Login_Time >= '$payDate 00:00:00' AND Login_Time <= '$payDate 23:59:59'");
};
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="styleHome.css" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Result Search Cus</title>
</head>

<body>
    <center>
        <div style="margin-bottom:10px;">
            <a href="Home.php">
                <button type="submit" style="height:30px" id="backBtn">
                    Back
                </button>
            </a>
        </div>
        <center>
            <table id="myTable">
                <thead>
                    <tr class="header">
                        <th>Customer Number</th>
                        <th>Phone</th>
                        <th>Card ID</th>
                        <th>Pay Date</th>
                        <th>Login Time</th>
                        <th>Logout Time</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                while($row = $searchCus->fetch_assoc()){
                echo "<tr>"; 
                echo "<td>".$row['Cus_No']."</td>".
                     "<td>".$row['Phone']."</td>".
                     "<td>".$row['Card_ID']."</td>".
                     "<td>".$row['P_Date']."</td>".
                     "<td>".$row['Login_Time']."</td>".
                     "<td>".$row['Logout_Time']."</td>".
                     "<td>".$row['Emp_ID']."</td>";
                echo "</tr>";
                    }
            ?>
                </tbody>
            </table>
</body>

</html>